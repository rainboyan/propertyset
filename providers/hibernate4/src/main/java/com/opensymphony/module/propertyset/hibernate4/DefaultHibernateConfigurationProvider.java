/*
 * Copyright (c) 2002-2003 by OpenSymphony
 * All rights reserved.
 */
package com.opensymphony.module.propertyset.hibernate4;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.Iterator;
import java.util.Map;


public class DefaultHibernateConfigurationProvider
        implements com.opensymphony.module.propertyset.hibernate4.HibernateConfigurationProvider
{
    //~ Instance fields ////////////////////////////////////////////////////////

    private Configuration configuration;
    private com.opensymphony.module.propertyset.hibernate4.HibernatePropertySetDAO propertySetDAO;
    private SessionFactory sessionFactory;

    //~ Methods ////////////////////////////////////////////////////////////////

    public void setConfiguration(Configuration configuration)
    {
        this.configuration = configuration;
    }

    public Configuration getConfiguration()
    {
        return configuration;
    }

    public com.opensymphony.module.propertyset.hibernate4.HibernatePropertySetDAO getPropertySetDAO()
    {
        if (propertySetDAO == null)
        {
            propertySetDAO = new com.opensymphony.module.propertyset.hibernate4.HibernatePropertySetDAOImpl(sessionFactory);
        }

        return propertySetDAO;
    }

    public void setSessionFactory(SessionFactory sessionFactory)
    {
        this.sessionFactory = sessionFactory;
    }

    public void setupConfiguration(Map configurationProperties)
    {
        // loaded hibernate config
        try
        {
            configuration = new Configuration().addClass(PropertySetItemImpl.class);

            Iterator itr = configurationProperties.keySet().iterator();

            while (itr.hasNext())
            {
                String key = (String) itr.next();

                if (key.startsWith("hibernate4"))
                {
                    configuration.setProperty(key, (String) configurationProperties.get(key));
                }
            }

            this.sessionFactory = configuration.buildSessionFactory();
        }
        catch (HibernateException e)
        {
        }
    }
}
