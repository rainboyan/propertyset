/*
 * Copyright (c) 2002-2003 by OpenSymphony
 * All rights reserved.
 */
package com.opensymphony.module.propertyset.memory;

import com.opensymphony.module.propertyset.AbstractPropertySetTest;
import org.junit.Before;


/**
 * User: bbulger
 * Date: May 22, 2004
 */
public class MemoryPropertySetTest extends AbstractPropertySetTest {
    //~ Methods ////////////////////////////////////////////////////////////////

    @Before
    public void setUp() throws Exception {
        ps = new MemoryPropertySet();
        ps.init(null, null);
    }
}
