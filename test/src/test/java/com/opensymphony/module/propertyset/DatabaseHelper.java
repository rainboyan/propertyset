/*
 * Copyright (c) 2002-2003 by OpenSymphony
 * All rights reserved.
 */
package com.opensymphony.module.propertyset;

import com.mckoi.database.jdbc.MSQLException;
import com.opensymphony.module.propertyset.database.JDBCPropertySetTest;
import com.opensymphony.util.ClassLoaderUtil;
import junit.framework.Assert;
import net.sf.hibernate.cfg.Configuration;
import net.sf.hibernate.tool.hbm2ddl.SchemaExport;
import org.apache.commons.dbcp.cpdsadapter.DriverAdapterCPDS;
import org.apache.commons.dbcp.datasources.SharedPoolDataSource;
import org.ofbiz.core.entity.util.ClassLoaderUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;


/**
 * @author Eric Pugh
 *         <p>
 *         This helper class populates a test mckoi database.
 */
public class DatabaseHelper
{
    //~ Static fields/initializers /////////////////////////////////////////////

    private static final Logger log = LoggerFactory.getLogger(DatabaseHelper.class);

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     * Create the database by loading a URL pointing at a SQL script.
     */
    public static void createDatabase(URL url)
    {
        Assert.assertNotNull("Database url is null", url);

        try
        {
            String sql = getDatabaseCreationScript(url);
            createDatabase(sql);
        }
        catch (IOException e)
        {
            log.error(e.getMessage(), e);
        }
    }

    public static void setupJNDIDataSource() throws Exception
    {
        System.setProperty(Context.INITIAL_CONTEXT_FACTORY, "org.apache.naming.java.javaURLContextFactory");
        System.setProperty(Context.URL_PKG_PREFIXES, "org.apache.naming");

        if (!isContextSetup())
        {
            URL mckoiResource = ClassLoaderUtils.getResource("mckoi.conf", JDBCPropertySetTest.class);

            SharedPoolDataSource dataSource = new SharedPoolDataSource();
            DriverAdapterCPDS cpds = new DriverAdapterCPDS();
            cpds.setDriver("com.mckoi.JDBCDriver");
            cpds.setUrl("jdbc:mckoi:local://" + mckoiResource.getFile() + "?create=true");
            cpds.setUser("test");
            cpds.setPassword("test");

            dataSource.setConnectionPoolDataSource(cpds);
            dataSource.setMaxActive(10);
            dataSource.setMaxWait(50);

            // Create initial context
            InitialContext ic = new InitialContext();

            ic.createSubcontext("jdbc");

            ic.bind("jdbc/DefaultDS", dataSource);
        }

        DatabaseHelper.createDatabase(ClassLoaderUtil.getResource("mckoi.sql", DatabaseHelper.class));
    }

    private static boolean isContextSetup()
    {
        try
        {
            InitialContext ic = new InitialContext();
            ic.lookup("jdbc/DefaultDS");
            return true;
        }
        catch (NamingException e)
        {
            return false;
        }
    }


    /**
     * Create a new database and initialize it with the specified sql script.
     *
     * @param sql the sql to execute
     */
    public static void createDatabase(String sql)
    {
        Connection connection;
        Statement statement = null;
        String sqlLine = null;

        try
        {
            InitialContext context = new InitialContext();
            DataSource ds = (DataSource) context.lookup("jdbc/DefaultDS");

            connection = ds.getConnection();
            statement = connection.createStatement();

            String[] sqls = sql.split(";");

            for (int i = 0; i < sqls.length; i++)
            {
                sqlLine = sqls[i].trim();
                sqlLine.replace("\r", "");
                sqlLine.replace("\n", "");

                //String s = sqls[i];
                if ((sqlLine.length() > 0) && (sqlLine.charAt(0) != '#'))
                {
                    try
                    {
                        statement.executeQuery(sqlLine);
                    }
                    catch (MSQLException e)
                    {
                        if (sqlLine.toLowerCase().indexOf("drop") == -1)
                        {
                            log.error("Error executing " + sqlLine, e);
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            log.error("Database creation error.  sqlLine:" + sqlLine, e);
        }
        finally
        {
            if (statement != null)
            {
                try
                {
                    statement.close();
                }
                catch (Exception ex)
                {
                    //not catch
                }
            }
        }
    }

    /**
     * Use the default Hibernate *.hbm.xml files.  These build the primary keys
     * based on an identity or sequence, whatever is native to the database.
     *
     * @throws Exception
     */
    public static Configuration createHibernateConfiguration() throws Exception
    {
        Configuration configuration = new Configuration();

        //cfg.addClass(HibernateHistoryStep.class);
        URL propertySet = DatabaseHelper.class.getResource("/com/opensymphony/module/propertyset/hibernate/PropertySetItemImpl.hbm.xml");
        Assert.assertTrue(propertySet != null);
        configuration.addURL(propertySet);
        configuration.setProperty("hibernate.dialect", "net.sf.hibernate.dialect.MckoiDialect");
        new SchemaExport(configuration).create(false, true);

        return configuration;
    }

    private static String getDatabaseCreationScript(URL url) throws IOException
    {
        InputStreamReader reader = new InputStreamReader(url.openStream());
        StringBuffer sb = new StringBuffer(100);
        int c = 0;

        while (c > -1)
        {
            c = reader.read();

            if (c > -1)
            {
                sb.append((char) c);
            }
        }

        return sb.toString();
    }
}
